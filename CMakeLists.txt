cmake_minimum_required(VERSION 2.8.12)

project(compile-for-gocd-example CXX)

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++98 -Wall -pedantic -Wextra")

add_executable(compile-for-gocd-example main.cpp)
